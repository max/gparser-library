#include "gparser.h"

/*
 *
 *	Create an " EXPRESSION " node
 *	make pointing is childs to the current element node
 *	and insert into the list
 *
 * 	head 	=> list 
 *	e	=> node reference
 *
 * 	return 0 on success otherwise memory alloc failed
 */
static
int 	create_expr(t_node **head, t_node *e)
{
	t_node *p;
	t_node *n;
	t_node *expr;

	p = e->prev;
	n = e->next;

	expr = create_node(0);
	if (!expr)
		return -400;
	SET_EXPR(expr->elem);
	expr->childs[0] = p;
	expr->childs[1] = e;
	expr->childs[2] = n;

	insert_node_before(head, p ? p : e, expr);
	return 0;
}
/*
 *
 *
 * 	Build the tree givin' the input token list 
 *
 * 	the first node of the input list will be the head of the tree
 *	
 *	other nodes are stored to the backup list
 *
 *
 * 	head 	=> in/out 	input token list 
 * 	backup 	=> in/out	keep trace of the node 
 * 		
 * 	return 0 on success otherwise memory alloc failed
 *
 */
static
int 	create_tree(t_node **head, t_node **backup)
{
	t_node *p;
	t_node *e;
	t_node *n;
	t_node *list;
	int 	count;
	int 	retalloc;

	retalloc = 0;
	list = 0;
	count = count_list_node(head);

	while (--count > -1)
	{
		e = get_list_node_index(*head, count);
		if (e)
		{
			p = e->prev;
			n = e->next;
		}
		if (e && G_WOPCODE(e))
		{
			if (p && n && G_OPEN(p) && G_CLOSE(n))
			{
				remove_node(head, p);
				remove_node(head, n);
				count++;
			}
		}
		else if (e && G_WOPERATOR(e))
		{
			if (p && n && G_WOPCODE(p) && G_WOPCODE(n))
			{
				if ((retalloc = create_expr(head, e)) != 0)
					goto end;
				exchange_list_node(head, n, &list);
				exchange_list_node(head, e, &list);
				exchange_list_node(head, p, &list);
			}
		}
	}
	*backup = list;
end :
	return retalloc;
}

/*
 *	Check rules for tokens in the tokens list
 *	
 *
 * 	e 	=> element to check
 *
 *	return 0 on success otherwise negative value
 *
 */
static
int 	verify_rules(t_node *e)
{
	t_node *p;
	t_node *n;

	p = e->prev;
	n = e->next;
	if (G_WOPCODE(e))
	{
		if (e->prev)
		{
			if (G_WOPCODE(p))
				return -1;
			if (G_WPARENTHESIS(p) && G_CLOSE(p))
				return -2;
		}
		if (e->next)
		{
			if (G_WOPCODE(n))
				return -3;
			if (G_WPARENTHESIS(n) && G_OPEN(n))
				return -4;
		}
	}
	else if (G_WOPERATOR(e))
	{
		if (!e->prev || !e->next)
			return -5;
		if (G_WOPERATOR(p) || G_WOPERATOR(p))
			return -6;
		if (G_WPARENTHESIS(p) && G_OPEN(p))
			return -7;
		if (G_WPARENTHESIS(n) && G_CLOSE(n))
			return -8;
	}
	else if (G_WPARENTHESIS(e))
	{
		if (G_OPEN(e))
		{
			if (p && (G_WOPCODE(p) || (G_WPARENTHESIS(p) && G_CLOSE(p))))
				return -9;
			if (!n)
				return -10;
			if ((G_WPARENTHESIS(n) && G_CLOSE(n) ) || G_WOPERATOR(n))
				return -11;
		}
		else if (G_CLOSE(e))
		{
			if (!p)
				return -12;
			if (G_WOPERATOR(p) || (G_WPARENTHESIS(p) && G_OPEN(p)))
				return -13;
			if (n && ((G_WPARENTHESIS(n) && G_OPEN(n)) || G_WOPCODE(n)))
				return -14;
		}
	}
	return 0;
}
/*
 *
 *	Take the tokens list and verify rules for tokens
 *	
 *	start 	=> the token list
 *
 *	return 0 on success otherwise negative value
 *
 */

static
int 	check_list_node(t_node **start)
{
	int res;
	int checkparenthesis;
	int checkopcode;
	t_node *e;

	e = *start;
	res = 0;
	checkopcode = 0;
	checkparenthesis = 0;
	while (e)
	{
		res = verify_rules(e);
		if (res != 0)
			return (res + (-100));
		if (G_WPARENTHESIS(e) && G_OPEN(e))
			checkparenthesis++;
		if (G_WPARENTHESIS(e) && G_CLOSE(e))
			checkparenthesis--;
		if (G_WOPCODE(e))
			checkopcode++;
		e = e->next;
	}
	if (checkparenthesis != 0)
		return -15;
	if (checkopcode == 0)
		return -16;
	return 0;
}
/*
 *
 * 	Extract the value of the token setted in the input string into the node
 *
 *	str 	=> input string to extract value
 *	e 	=> node will contain the extracted value
 *	sz 	=> size of data extracted
 *
 *	return 0 on success otherwise negative value
 */

static
int extract_value(char *str, t_node *e, size_t *sz)
{
	char *h;
	char value[256];

	TRIM_WHITE_SPACE(str);	

	if (!str)
	{
		return -1;
	}
	if (!(str + 1))
	{
		if (*str != ')')
		{
			return -2;
		}
	}
	if (G_WOPCODE(e) && *str != '(')
		return -3;
	else
	{
		str++;
		if (!str) 
			return -4;
		h = str;
		str = strchr(str, ')');
		if (!str) 
			return -5;
		if (h == str + 1)
			return -6;

		*sz = (size_t) (str - h);
		if (*sz >= 255)
		{
			return -7;
		}
		(*sz)++;
		memset(value, 0, sizeof(value));
		memcpy(value, h, (size_t)(str - h));

		e->value = strdup(value);
		if (!e->value)
			return -400;

		if (*sz == 1)
			return -8;
	}
	return 0;
}
/*
 *
 *
 * 	Build the token list and call to build the tree givin' the input string
 *
 * 	str 	=> input str
 * 	tree 	=> pointer on tree, init to 0 (Zero) , will contain the root node 
 * 	backup 	=> pointer on tokens list , init to 0 (Zero), will contain all tokens list
 *	
 *	return 0 on success otherwise negative value
 *
 */

int 	gbuild(char *str, t_node **tree, t_node **backup)
{
	size_t len;
	size_t i;
	size_t cmpstrlen;
	size_t sz;
	int res;
	t_node *e;
	t_node *head;

	e = 0;
	head = 0;
	res = -1;

	len = sizeof(tokens) / sizeof(tokens[0]);

	while (str && *str)
	{
		TRIM_WHITE_SPACE(str);	
		i = 0;
		while (i < len)
		{
			cmpstrlen = strlen(tokens[i].str);
			if (!memcmp(str, tokens[i].str, cmpstrlen))
			{
				e = create_node(0);
				if (!e)
				{
					return -400;
				}
				e->elem = tokens[i].elem;
				if (head == 0) 
				{
					add_to_head(&head, e);
					*tree = head;
				}
				else
				{
					add_to_queue(&head, e);
				}
				if (G_WOPCODE(e))
				{
					sz = 0;
					res = extract_value(str + cmpstrlen, e, &sz);
					if (res < 0)
					{
						return (res += (-200));
					}
					else
					{
						str += cmpstrlen;
						str += (sz == 0 ? (size_t)res : sz);
					}
				}
				else if (G_WOPERATOR(e) || G_WPARENTHESIS(e))
					str += cmpstrlen - 1;
				break;
			}
			i++;
		}
		if (i >= len)
		{
			return -300;
		}
		str++;
	}
	res = check_list_node(&head);
	if (res == 0)
	{
		create_tree(tree, backup);
	}
	return res;
}
