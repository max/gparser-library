
#include "gparser.h"

void print_node_dbg(t_node *e)
{
	if (IS_LEAF(e->elem))
		printf("LEAF");
	else
		printf("NOT LEAF");

	printf("\n");

	if (IS_WOPCODE(e->elem))
		printf("WORD_OPCODE");
	if (IS_WOPERATOR(e->elem))
		printf("WORD_OPERATOR");
	if (IS_WPARENTHESIS(e->elem))
		printf("WORD_PARENTHESIS");

	printf("\n");

	if (IS_SIG(e->elem))
		printf("SIG");
	if (IS_XHX(e->elem))
		printf("XHX");
	if (IS_CLTV(e->elem))
		printf("CLTV");
	if (IS_CSV(e->elem))
		printf("CSV");
	if (IS_EXPR(e->elem))
		printf("EXP");

	printf("\n");

	if (IS_OPEN(e->elem))
		printf("OPEN ");
	if (IS_CLOSE(e->elem))
		printf("CLOSE ");
	printf("\n");
	if (IS_AND(e->elem))
		printf("AND");
	if (IS_OR(e->elem))
		printf("OR");
}

void print_node(t_node *e)
{
	printf("Leaf: %s\n", IS_LEAF(e->elem) ? "true": "false");
	printf ("Confirmed: %d\n" , e->confirmed);
	printf("Type: ");
	if (IS_WOPCODE(e->elem))
	{
		printf("WORD_OPCODE");
		printf("\n");
		printf("Op: ");
		if (IS_SIG(e->elem))
			printf("SIG");
		if (IS_XHX(e->elem))
			printf("XHX");
		if (IS_CLTV(e->elem))
			printf("CLTV");
		if (IS_CSV(e->elem))
			printf("CSV");
		if (IS_EXPR(e->elem))
			printf("EXP");
		if (e->value != 0)
			printf(" { %s }", e->value);
	}
	else if (IS_WOPERATOR(e->elem))
	{
		printf("WORD_OPERATOR");
		printf("\n");
		printf("Op: ");
		if (IS_AND(e->elem))
			printf("AND");
		if (IS_OR(e->elem))
			printf("OR");
	}
	else if (IS_WPARENTHESIS(e->elem))
	{
		printf("WORD_PARENTHESIS");
		printf("\n");
		printf("Op: ");
		if (IS_OPEN(e->elem))
			printf("OPEN ");
		if (IS_CLOSE(e->elem))
			printf("CLOSE ");
	}

	printf("\n");
}

void print_node_depth(t_node *e, int depth)
{
	size_t len;
	size_t i;

	len = sizeof(tokens) / sizeof(tokens[0]);
	i = 0;
	while (i < len)
	{
		if (tokens[i].elem == e->elem)
		{
			while (--depth > -1)
				printf("\t");
			if (e->confirmed)
				printf("✅ ");
			else
				printf("❌ ");
			
			printf("%s", tokens[i].str);
			if (e->value)
				printf(" %s", e->value);
			printf("\n");
			return;
		}
		i++;
	}
}
