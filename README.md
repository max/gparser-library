
Library made to parse and build tree to verify and construct ' unlock ' string for Multisig operation.

Example of how to use in the main.c file

Test :

make && make clean && gcc main.c -L . -lgparser -o GPARSER_TEST && ./GPARSER_TEST

### Notes

This was build in 2017 ( in Javascript )  
![Image of application developped for multi sig operation](https://forum.duniter.org/uploads/default/optimized/2X/3/385f1fbbb1ae460835af378d4aff38cd4c71c62a_2_315x500.png)

and provided functionality to make Multi [operation - lock & unlock ] for transactions through software like Duniter to try the BNF grammar implementation.


Now, part of the logic, programmed in C to let developpers integrate it in their privilegied language if they want.

It can be bind for C, C++, Rust, Java, Python, Swift ... based projects such as Duniter / Juniter / Durs / Silkaj / Sakjia / Cesium and many more.

