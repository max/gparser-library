#include "gparser.h"
#include "colors.h"
#include <string.h>
#include "test.h"
	/*
	 * 	=> Callback is triggered on a leaf node
	 *
	 * 	function assigned in the main to the t_gparser variable 
	 *
	 * 	you have to provide your test to set the tree 
	 *
	 * 	and then set the node as confirmed or not
	 *
	 * 	by defaut node is set to ZERO : e->confirmed = 0
	 *
	 *
	 *	e->confirmed = 1 where  e->value == v 
	 *
	 *	note : 	e->value has been allocated on the heap
	 *		remember to free memory if replace
	 *
	 * */
void 	called_on_leaf(t_node *e)
{
	/*
	 *	Provide your value !
	 *
	 */
	char *v = 0;

	if (IS_SIG(e->elem))
	{
	/*
	 *	where e->value and v  = base 58 string 
	 *
	 * */
	//	if (!memcmp(e->value, v, strlen(e->value)))
			e->confirmed = 1;
	}
	else if (IS_XHX(e->elem))
	{
	/*
	 *	where e->value == sha256(v) 
	 *
	 * */
	//	if ( !memcmp(e->value, sha256(v), strlen(e->value))
			e->confirmed = 1;
	}
	else if (IS_CLTV(e->elem))
	{
	/*
	 *	where v >= e->value
	 *
	 *	 transform e->value and v to number
	 *	 as epoch
	 *
	 * */
		e->confirmed = 1;	
	}
	else if (IS_CSV(e->elem))
	{
	/*
	 *	where v >= e->value
	 *
	 *	? unkwown since time block !
	 *
	 * */
	//	e->confirmed = 1;	
	}
}



int main(int ac, char **av)
{

#define BUFF_SIZE 8192
	char buf[BUFF_SIZE];
	int ret;
	t_gparser parser;



	// Set the callback !
	parser.callback = called_on_leaf;

	if (ac == 2 && (*(av[1]) ) =='1')
	{
		while (1)
		{
			memset(buf, 0, BUFF_SIZE);
			ret = read(0, buf, BUFF_SIZE);
			if (ret > 0)
			{
				buf[ret] = 0;
				printf("%s", COLOR_CYAN);
				printf("	------	INPUT STRING \n>> test = [ %s ]\n\n", buf);
				printf("%s", COLOR_RESET);
				ret = gparser(buf, &parser);
				printf(">>> GPARSER RESULT %d \n", ret);
				ret  = gparser_confirmed(&parser);
				printf(">>> GPARSER CONFIRMED ? %s\n", ret == 1 ? "true" : "false");
				if (ret == 1)
				{
					printf("%s>>> GPARSER TREE CONFIRMED - PRINTING :%s\n", COLOR_YELLOW, COLOR_RESET);
				}
					gparser_print(&parser);

				gparser_free(&parser);
			}
			if (ret <= 0)
				break;
		}
		return 0;
	}


	/*
	 *
	 *	Test the parser
	 *
	 */
	printf("\t\tTESTING PARSER\n");
	int i = -1;
	int len = sizeof(tests) / sizeof(tests[0]);

	while (++i < len)
	{
		printf("%s", COLOR_CYAN);
		printf("\n>> test = %d [ %s ]\n\n", i, tests[i].teststr);
		printf("%s", COLOR_RESET);

		ret = gparser((char*)tests[i].teststr, &parser);
		printf(">>> GPARSER RESULT %d \n", ret);

		if (tests[i].parsing == PARSER_FAIL)
		{
			if (ret != 1) 
				printf("%sTEST OK\n",  COLOR_GREEN );
			else
				printf("%sTEST FAIL\n",  COLOR_RED );
		}
		else if (tests[i].parsing == PARSER_OK)
		{
			if (ret == 0) 
				printf("%sTEST OK\n",  COLOR_GREEN );
			else
				printf("%sTEST FAIL\n",  COLOR_RED );
		}
		printf("%s\n", COLOR_RESET);
		gparser_free(&parser);
	
	}
	/*
	 *
	 *	Test the tree with confirmed value
	 *
	 */
	printf("\t\tTESTING TREE\n");
	i = -1;
	len = sizeof(tests) / sizeof(tests[0]);

	while (++i < len)
	{
		if (tests[i].parsing == PARSER_FAIL)
			continue;
		if (!tests[i].testcallback)
		{
			printf("Should be an error in your stress test !\n");
			return -1;
		}
		else
			parser.callback = tests[i].testcallback;

		printf("%s", COLOR_CYAN);
		printf("\n>> test = %d [ %s ]\n\n", i, tests[i].teststr);
		printf("%s", COLOR_RESET);
		
		ret = gparser((char*)tests[i].teststr, &parser);
		printf(">>> GPARSER RESULT %d \n", ret);
		
		ret  = gparser_confirmed(&parser);
		printf(">>> GPARSER CONFIRMED ? %s \n", ret == 1 ? "true" : "false");
		
		printf("%s", COLOR_YELLOW);
		if (ret == 1)
			printf(">>> GPARSER CONFIRMED - PRINTING \n");
		printf("%s", COLOR_RESET);


		gparser_print(&parser);
		gparser_free(&parser);
		
		printf("%s", ret == 1 ? COLOR_GREEN : COLOR_RED);
		printf("\n>> test = %d [ %s ]\n\n\n", i, tests[i].teststr);
		printf("%s", COLOR_RESET);
		if (ret == tests[i].tree)
		{
			printf("%sTEST OK\n",  COLOR_GREEN );
		}
		else
		{
			printf("%sTEST FAIL\n",  COLOR_RED );
		}
		printf("%s", COLOR_RESET);
		printf("\n\n");
	}

	return 0;
}
/*
 *
 *	some tests !
 *
 */
void sig_to_zero(t_node *e)
{
	if (!IS_SIG(e->elem))
		e->confirmed = 1;
}
void xhx_to_zero(t_node *e)
{
	if (!IS_XHX(e->elem))
		e->confirmed = 1;
}
void cltv_to_zero(t_node *e)
{
	if (!IS_CLTV(e->elem))
		e->confirmed = 1;
}
void csv_to_zero(t_node *e)
{
	if (!IS_CSV(e->elem))
		e->confirmed = 1;
}
/*
 *
 *
 *
 */

void sig_xhx_to_zero(t_node *e)
{
	sig_to_zero(e);
	if (IS_XHX(e->elem))
		e->confirmed = 0;
}
