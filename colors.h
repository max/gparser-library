
#ifndef _COLORS_H_
#define _COLORS_H_
#define COLOR_GREEN "\x1B[32m"
#define COLOR_RED "\x1B[31m" 
#define COLOR_YELLOW "\x1B[33m"
#define COLOR_CYAN "\x1B[36m"
#define COLOR_BLUE "\x1B[34m"
#define COLOR_RESET "\x1B[0m"
#endif
