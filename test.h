#ifndef _TEST_H_

#define _TEST_H_

#include "gparser.h"

typedef struct s_test t_test;

#define PARSER_FAIL 0
#define PARSER_OK 1 

#define TREE_FAIL 0
#define TREE_OK 1


struct s_test{
	int parsing;
	int tree;
	char *teststr;
	t_fn_callback testcallback;
};

void sig_to_zero(t_node *e);
void xhx_to_zero(t_node *e);
void cltv_to_zero(t_node *e);
void csv_to_zero(t_node *e);

void sig_xhx_to_zero(t_node *e);

static const t_test tests[]={
	/*
	 *
	 *
	 *	TEST CASE ERROR PARSING 
	 *
	 * */

	{PARSER_FAIL, 0,	"", 0 },
	{PARSER_FAIL, 0,	"1", 0},
	{PARSER_FAIL, 0,	"abcde", 0},
	{PARSER_FAIL, 0,	"SIG", 0},
	
	{PARSER_FAIL, 0,	"SIG()", 0},
	{PARSER_FAIL, 0,	"XHX()", 0},
	{PARSER_FAIL, 0,	"CSV()", 0},
	{PARSER_FAIL, 0,	"CLTV()", 0},

	{PARSER_FAIL, 0,	"aSIG(0)", 0},
	{PARSER_FAIL, 0,	"a SIG(0)", 0},
	{PARSER_FAIL, 0,	"SIG(0)a", 0},
	{PARSER_FAIL, 0,	"SIG(0) a", 0},
	{PARSER_FAIL, 0,	"aSIG(0)a", 0},
	{PARSER_FAIL, 0,	"a SIG(0) a", 0},
	{PARSER_FAIL, 0,	"  SIG(abcdef) && )", 0},
	// considering error if data after last close parenthesis ) 
	{PARSER_FAIL, 0,	"  SIG(abcdef) ", 0},
	{PARSER_FAIL, 0,	"SIG(abcdef) ", 0},


	// wrong string
	{PARSER_FAIL, TREE_FAIL,	"CSV() && (SIG(123) && CLTV(1454))", &cltv_to_zero} ,
	{PARSER_FAIL, TREE_FAIL,	"CSV(g) && && (SIG(123) && CLTV(1454))", &cltv_to_zero} ,
	{PARSER_FAIL, TREE_FAIL,	"((( SIG(1) && CLTVV(2)) || XHX(3) ) && CSV(4) ) || (SIG(5) && ((CLTV(7) || XHX(8) )))", &sig_xhx_to_zero},	
	{PARSER_FAIL, TREE_FAIL,	"(((( SIG(1) && CLTV(2)) || XHX(3) || XHX(A3)) || SIG(33) && CSV(4) && CSV(333) ) && SIG(333B) || (SIG(5) && ((CLTV(7) || XHX(8) )) && (( SIG(11) && CLTV(22)) || XHX(33) ) && CSV(44) ) || (SIG(55) && ((CLTV(77) || XHX(88) ))) && CSV(23] )", &sig_xhx_to_zero},

	/*
	 *
	 *
	 *	TEST CASE PARSING OK 
	 *
	 *	change TREE_OK <-> TREE_FAIL for testing case !
	 *
	 * */
	{PARSER_OK, TREE_FAIL,	"SIG(1)", &sig_to_zero},
	{PARSER_OK, TREE_OK,	"XHX(1)", &sig_to_zero},
	{PARSER_OK, TREE_OK,	"CLTV(1)", &sig_to_zero},
	{PARSER_OK, TREE_OK,	"CSV(1)", &sig_to_zero},

	{PARSER_OK, TREE_OK,	"SIG(1)", &xhx_to_zero},
	{PARSER_OK, TREE_FAIL,	"XHX(1)", &xhx_to_zero},
	{PARSER_OK, TREE_OK,	"CLTV(1)", &xhx_to_zero},
	{PARSER_OK, TREE_OK,	"CSV(1)", &xhx_to_zero},
	
	{PARSER_OK, TREE_OK,	"SIG(1)", &cltv_to_zero },
	{PARSER_OK, TREE_OK,	"XHX(1)", &cltv_to_zero },
	{PARSER_OK, TREE_FAIL,	"CLTV(1)", &cltv_to_zero },
	{PARSER_OK, TREE_OK,	"CSV(1)", &cltv_to_zero },

	
	{PARSER_OK, TREE_OK,	"SIG(1)", &csv_to_zero },
	{PARSER_OK, TREE_OK,	"XHX(1)", &csv_to_zero },
	{PARSER_OK, TREE_OK,	"CLTV(1)", &csv_to_zero },
	{PARSER_OK, TREE_FAIL,	"CSV(1)", &csv_to_zero },



	{PARSER_OK, TREE_OK,	"((SIG(1) || CSV(2)) && CSV(22)) && (CLTV(3) || XHX(4) && SIG(5))", &sig_to_zero},	
	{PARSER_OK, TREE_OK,	"((SIG(1) || CSV(2)) && CSV(22)) && (CLTV(3) || XHX(4) && SIG(5))", &xhx_to_zero},	
	{PARSER_OK, TREE_OK,	"((SIG(1) || CSV(2)) && CSV(22)) && (CLTV(3) || XHX(4) && SIG(5))", &cltv_to_zero},	
	{PARSER_OK, TREE_OK,	"((SIG(1) || CSV(2)) && CSV(22)) && (CLTV(3) || XHX(4) && SIG(5))", &sig_to_zero},	


	{PARSER_OK, TREE_FAIL,	"CSV(g) && (SIG(123) && CLTV(1454))", &sig_to_zero} ,
	{PARSER_OK, TREE_FAIL,	"CSV(g) && (SIG(123) && CLTV(1454))", &cltv_to_zero} ,
	{PARSER_OK, TREE_OK,	"CSV(g) && (SIG(123) && CLTV(1454))", &xhx_to_zero} ,
	{PARSER_OK, TREE_FAIL,	"CSV(g) && (SIG(123) && CLTV(1454))", &sig_xhx_to_zero} ,


	{PARSER_OK, TREE_OK,	"(SIG(1) || CSV(2)) && (CLTV(3) || XHX(4) && SIG(5))", &csv_to_zero},	
	{PARSER_OK, TREE_OK,	"(SIG(1) || CSV(2)) && (CLTV(3) || XHX(4) && SIG(5))", &sig_to_zero},	
	{PARSER_OK, TREE_OK,	"(SIG(1) || CSV(2)) && (CLTV(3) || XHX(4) && SIG(5))", &xhx_to_zero},	
	{PARSER_OK, TREE_OK,	"(SIG(1) || CSV(2)) && (CLTV(3) || XHX(4) && SIG(5))", &sig_xhx_to_zero},	

	{PARSER_OK, TREE_FAIL,	"((( SIG(1) && CLTV(2)) || XHX(3) ) && CSV(4) ) || (SIG(5) && ((CLTV(7) || XHX(8) )))", &sig_xhx_to_zero},	
	{PARSER_OK, TREE_OK,	"((( SIG(1) && CLTV(2)) || XHX(3) ) && CSV(4) ) || (SIG(5) && ((CLTV(7) || XHX(8) )))", &csv_to_zero},	
	{PARSER_OK, TREE_OK,	"((( SIG(1) && CLTV(2)) || XHX(3) ) && CSV(4) ) || (SIG(5) && ((CLTV(7) || XHX(8) )))", &sig_to_zero},	
	{PARSER_OK, TREE_OK,	"((( SIG(1) && CLTV(2)) || XHX(3) ) && CSV(4) ) || (SIG(5) && ((CLTV(7) || XHX(8) )))", &cltv_to_zero},	


	{PARSER_OK, TREE_OK,	"(((( SIG(1) && CLTV(2)) || XHX(3) || XHX(A3)) || SIG(33) && CSV(4) && CSV(333) ) && SIG(333B) || (SIG(5) && ((CLTV(7) || XHX(8) )) && (( SIG(11) && CLTV(22)) || XHX(33) ) && CSV(44) ) || (SIG(55) && ((CLTV(77) || XHX(88) ))) && CSV(23) )", &csv_to_zero},
	{PARSER_OK, TREE_FAIL,	"(((( SIG(1) && CLTV(2)) || XHX(3) || XHX(A3)) || SIG(33) && CSV(4) && CSV(333) ) && SIG(333B) || (SIG(5) && ((CLTV(7) || XHX(8) )) && (( SIG(11) && CLTV(22)) || XHX(33) ) && CSV(44) ) || (SIG(55) && ((CLTV(77) || XHX(88) ))) && CSV(23) )", &sig_xhx_to_zero},
	{PARSER_OK, TREE_FAIL,	"(((( SIG(1) && CLTV(2)) || XHX(3) || XHX(A3)) || SIG(33) && CSV(4) && CSV(333) ) && SIG(333B) || (SIG(5) && ((CLTV(7) || XHX(8) )) && (( SIG(11) && CLTV(22)) || XHX(33) ) && CSV(44) ) || (SIG(55) && ((CLTV(77) || XHX(88) ))) && CSV(23) )", &sig_to_zero},


};

#endif
