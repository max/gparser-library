#ifndef _GPARSER_H
#define _GPARSER_H
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BIT_SET(bx, v)		(v |= 1 << bx)
#define BIT_CLEAR(bx, v)	(v &= ~(1 << bx))
#define BIT_IS_SET(bx, v)	((v >> bx) & 1)

#define SET_WOPCODE(v)		(BIT_CLEAR(7, v), 	BIT_CLEAR(6, v))
#define SET_WOPERATOR(v)	(BIT_CLEAR(7, v), 	BIT_SET(6, v))
#define SET_WPARENTHESIS(v)	(BIT_SET(7, v), 	BIT_CLEAR(6, v))

#define SET_LEAF(v)		(BIT_SET(0, v))
#define UNSET_LEAF(v)		(BIT_CLEAR(0, v))

#define SET_OR(v)		(BIT_SET(1,v), 		SET_WOPERATOR(v), UNSET_LEAF(v))
#define SET_AND(v)		(BIT_CLEAR(1,v), 	SET_WOPERATOR(v), UNSET_LEAF(v))

#define SET_OPEN(v)		(BIT_SET(2,v) , 	SET_WPARENTHESIS(v), UNSET_LEAF(v)) 
#define SET_CLOSE(v)		(BIT_CLEAR(2,v), 	SET_WPARENTHESIS(v), UNSET_LEAF(v)) 

#define SET_SIG(v)		(BIT_CLEAR(5, v), 	BIT_CLEAR(4, v),	BIT_CLEAR(3, v),	SET_WOPCODE(v), SET_LEAF(v))
#define SET_XHX(v)		(BIT_CLEAR(5, v), 	BIT_CLEAR(4, v),	BIT_SET(3, v),		SET_WOPCODE(v), SET_LEAF(v))
#define SET_CLTV(v)		(BIT_CLEAR(5, v), 	BIT_SET(4, v),		BIT_CLEAR(3, v),	SET_WOPCODE(v), SET_LEAF(v))
#define SET_CSV(v)		(BIT_CLEAR(5, v), 	BIT_SET(4, v),		BIT_SET(3, v),		SET_WOPCODE(v), SET_LEAF(v))
#define SET_EXPR(v)		(BIT_SET(5, v), 	BIT_CLEAR(4, v),	BIT_CLEAR(3, v),	SET_WOPCODE(v), UNSET_LEAF(v))

#define IS_LEAF(v)		(BIT_IS_SET(0, v))

#define IS_AND(v)		(!BIT_IS_SET(1, v))
#define IS_OR(v) 		(BIT_IS_SET(1, v))

#define IS_OPEN(v)		(BIT_IS_SET(2, v))
#define IS_CLOSE(v)		(!BIT_IS_SET(2, v))

#define IS_SIG(v)		(!BIT_IS_SET(5, v)	&& !BIT_IS_SET(4, v)	&& !BIT_IS_SET(3, v))
#define IS_XHX(v)		(!BIT_IS_SET(5, v)	&& !BIT_IS_SET(4, v)	&& BIT_IS_SET(3, v))
#define IS_CLTV(v)		(!BIT_IS_SET(5, v)	&& BIT_IS_SET(4, v)	&& !BIT_IS_SET(3, v))
#define IS_CSV(v)		(!BIT_IS_SET(5, v)	&& BIT_IS_SET(4, v)	&& BIT_IS_SET(3, v))
#define IS_EXPR(v)		(BIT_IS_SET(5, v)	&& !BIT_IS_SET(4, v)	&& !BIT_IS_SET(3, v))

#define IS_WOPCODE(v)		(!BIT_IS_SET(7, v)	&& !BIT_IS_SET(6, v))
#define IS_WOPERATOR(v) 	(!BIT_IS_SET(7, v)	&& BIT_IS_SET(6, v))
#define IS_WPARENTHESIS(v)	(BIT_IS_SET(7, v)	&& !BIT_IS_SET(6, v))

#define G_OPEN(e) 		(IS_OPEN(e->elem))
#define G_CLOSE(e) 		(IS_CLOSE(e->elem))
#define G_WOPCODE(e) 		(IS_WOPCODE(e->elem))
#define G_WOPERATOR(e) 		(IS_WOPERATOR(e->elem))
#define G_WPARENTHESIS(e) 	(IS_WPARENTHESIS(e->elem))

#define SIG 	0x01
#define XHX 	0x09
#define CLTV 	0x11
#define CSV 	0x19
#define AND 	0x40
#define OR 	0x42
#define CLOSE 	0x80
#define OPEN 	0x84

#define EXPR	0x20

#define TRIM_WHITE_SPACE(str) \
			while (str && *str == ' ')\
				str++;\

/*
	bit number:
	  7 6 		5 4 3		2	1 	0

	bit assignation :

	[ 0 0 		0 0 0 		0 	0 	1]

							- leaf : 
							0 false
							1 true

						- operator :
					      	0 AND
						1 OR

					- parenthesis :
					0 CLOSE
					1 OPEN 
			- opcode :
			0 0 0 SIG
			0 0 1 XHX
			0 1 0 CLTV
			0 1 1 CSV
			1 0 0 EXPR
	- type :
	0 0 WORD_OPCODE
	0 1 WORD_OPERATOR
	1 0 WORD_PARENTHESIS
*/



typedef struct s_node t_node;

struct s_node
{
	unsigned char 	elem;
	unsigned char 	confirmed;
	char 		*value;

	/*	use as tree 	*/
	t_node 		*childs[3];

	/*	use as token list	*/
	t_node 		*prev;
	t_node 		*next;
};	

typedef void(*t_fn_callback)(t_node *e);


typedef struct s_token
{
	char 		*str;
	unsigned char 	elem;
}			t_token;

static const t_token  tokens[8] ={
	{ "SIG",	SIG},
	{ "XHX",	XHX},
	{ "CLTV",	CLTV},
	{ "CSV",	CSV},
	{ "||",		OR},
	{ "&&",		AND},
	{ "(",		OPEN},
	{ ")",		CLOSE}
};

typedef struct s_gparser
{
	int 	parsing_result;
	t_node *tree;
	t_node *backup;
	t_fn_callback callback;
}	t_gparser;

int 	gparser(char *str, t_gparser *container);
int 	gparser_confirmed(t_gparser *container);
void 	gparser_print(t_gparser *container);
void 	gparser_free(t_gparser *container);
void 	gparser_iter(t_gparser *container);

int 	gbuild(char *str, t_node **head, t_node **backup);

t_node	*create_node(char *value);

void 	iter_list_node(t_node *start);
void 	iter_tree(t_node *e, int depth);
void 	iter_tree_callback(t_node *e, t_fn_callback call);
void 	iter_tree_confirmed(t_node *e, t_fn_callback call);

int 	count_list_node(t_node **h);
t_node 	*get_list_node_index(t_node *e, int c);


void 	insert_node_before(t_node **head, t_node *before, t_node *insert);
void 	remove_node(t_node **head, t_node *e);


void 	add_to_head(t_node **head, t_node *e);
void 	add_to_queue(t_node **head, t_node *e);
void 	exchange_list_node(t_node **head, t_node *e, t_node **other);


void 	print_node_dbg(t_node *e);
void 	print_node(t_node *e);
void 	print_node_depth(t_node *e, int depth);


void 	free_node(t_node *e);
void 	free_list_node(t_node **s);
#endif
