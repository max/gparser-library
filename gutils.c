
#include "gparser.h"

t_node *create_node(char *value)
{
	t_node *new;

	new = malloc(sizeof(*new));
	if (!new)
		return 0;
	memset(new, 0, sizeof(*new));
	new->value = value;
	return new; 
}

void iter_list_node(t_node *start)
{
	if (!start)
		return;
	while (start)
	{
		print_node(start);
		start = start->next;
	}
}
/*
 *
 *	Apply on the root node to traverse the tree
 *	and trigger callback only for confirmed node
 *
 *	e 	=> root node
 *	call 	=> callback function to trig
 *
 */

void 	iter_tree_confirmed(t_node *e, t_fn_callback call)
{
	t_node *left;
	t_node *right;

	if (!e || !call)
		return;
	if (IS_LEAF(e->elem) && e->confirmed == 1)
	{
		call(e);
		return;
	}
	left = e->childs[0];
	right = e->childs[2];

	if (e->confirmed == 1)
	{
		iter_tree_confirmed(left, call);
		iter_tree_confirmed(right, call);
	}
}

void 	iter_tree_callback(t_node *e, t_fn_callback call)
{
	t_node *left;
	t_node *right;
	t_node *op;

	if (!e || !call)
		return;
	if (IS_LEAF(e->elem))
	{
		call(e);
		return;
	}
	left = e->childs[0];
	op = e->childs[1];
	right = e->childs[2];
	iter_tree_callback(left, call);
	iter_tree_callback(right, call);

	e->confirmed = 0;
	op->confirmed = 0;
	if (IS_OR(op->elem))
	{
		if (left->confirmed || right->confirmed)
		{
			e->confirmed = 1;
			op->confirmed = 1;
		}
		return;
	}
	if (left->confirmed && right->confirmed)
	{
		e->confirmed = 1;
		op->confirmed = 1;
	}
}

void 	iter_tree(t_node *e, int depth)
{
	t_node *left;
	t_node *right;
	t_node *op;

	if (!e)
		return;
	print_node_depth(e, depth);
	left = e->childs[0];
	op = e->childs[1];
	right = e->childs[2];
	iter_tree(left, depth + 1);
	iter_tree(op, depth + 1);
	iter_tree(right, depth + 1);
}
/*
 *	Insert a node into the list before another node
 *
 *	head 	=> the list of the node
 *	before 	=> the node reference
 *	insert 	=> the node to insert into list
 *
 *
 */

void 	insert_node_before(t_node **head, t_node *e, t_node *insert)
{
	t_node *p;

	p = e->prev;
	if (!p)
	{ 
		insert->next = e;
		e->prev = insert;
		*head = insert;
	}
	else
	{
		p->next = insert;
		insert->prev = p;
		insert->next = e;
		e->prev = insert;
	}
}
/*
 *	Remove element e of the list head and set it into other list 
 *
 *	head 	=> list of primary node
 *	e 	=> node concerned
 *	other 	=> the second list where e will be inserted
 *
 *
 */

void 	exchange_list_node(t_node **head, t_node *e, t_node **other)
{
	t_node *p;
	t_node *n;

	p = e->prev;
	n = e->next;

	if (p)
	{
		p->next = e->next;
		if (n)
			n->prev = p;
	}
	else
	{
		*head = n;
		if (n)
			n->prev = 0;
	}
	add_to_head(other, e);
}
/*
 *
 *	Insert a node at head of the list
 *
 *	head 	=> the list of node
 *	e 	=> element to insert
 *
 */

void add_to_head(t_node **head, t_node *e)
{
	if (!e)
		return;
	if (*head == 0)
	{
		e->next = 0;
		e->prev = 0;
		*head = e;
		return;
	}
	e->prev = 0;
	e->next = *head;
	*head = e;
}
/*
 *
 *	Insert a node at the end of the list
 *
 *	head 	=> the list of node
 *	e 	=> element to insert
 *
 */
void 	add_to_queue(t_node **head, t_node *e)
{
	t_node *list;

	if (!e)
		return;
	list = *head;
	if (*head == 0)
	{
		e->next = 0;
		e->prev = 0;
		*head = e;
		return;
	}
	while (list && list->next)
		list = list->next;

	list->next = e;
	e->prev = list;
}
/*
 *
 *	Remove a node of the list, free allocated memory
 *
 *	head 	=> the list of node
 *	e 	=> element to remove
 *
 *
 */

void 	remove_node(t_node **head, t_node *e)
{
	t_node *p;
	t_node *n;

	p = e->prev;
	n = e->next;

	if (p)
	{
		p->next = e->next;
		if (n)
			n->prev = p;
	}
	else
	{
		*head = n;
		if (n)
			n->prev = 0;
	}
	free_node(e);
}
/*
 *
 *	Count how many elements inside list
 *
 */

int 	count_list_node(t_node **h)
{
	t_node *e;
	int i;

	e = *h;
	i = 0;
	if (!e)
		return 0;
	if (!e->next)
		return 1;
	while (e)
	{
		i++;
		e = e->next;
	}
	return i;
}

/*
 *
 *	get the n' element of the list
 *
 *
 */
t_node *get_list_node_index(t_node *e, int n)
{
	int i = -1;

	if (n <= 0)
		return 0;
	while (++i < n)
	{
		if (!e)
			return 0;
		e = e->next;
	}
	return e;
}

void free_node(t_node *e)
{
	if (!e)
		return;
	if (e->value)
	{
		free(e->value);
		e->value = 0;
	}
	free(e);
}

void free_list_node(t_node **s)
{
	t_node *n;
	t_node *start;

	if (!*s)
		return;
	start = *s;
	while (start)
	{
		n = start->next;
		if (start != 0)
		{
			free_node(start);
		}
		start = n; 
	}
	if (*s)
		*s = 0;
}

