
NAME		= libgparser.a


CC			= gcc
CFLAGS		= -Wall -Wextra -Werror -I includes -fPIC

FILES		= \
			gparser \
			gbuild \
			gutils \
			gprints \

SRC			= $(addsuffix .c, $(FILES))
OBJ			= $(addsuffix .o, $(FILES))

.PHONY: clean fclean re

.SILENT:

all: $(NAME)


$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)
	printf "\xF0\x9F\x8D\xBA  $@ is ok\n  "
	printf ""

%.o: %.c

	printf "\xF0\x9F\x92\xBF  $@\n"
	printf ""
	gcc $(CFLAGS) -c -o $@ $^

clean:
	rm -f $(OBJ)
	printf "\xE2\x8C\xA6  objects files \n"

fclean: clean
	rm -f $(NAME)
	printf "\xE2\x8C\xA6  $(NAME) file \n"

re: fclean all
