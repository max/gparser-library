#include "gparser.h"

void 	gparser_print(t_gparser *container)
{
	if (!container)
		return;
	gparser_iter(container);
}

void 	gparser_iter(t_gparser *container)
{
	if (!container)
		return;
	iter_tree(container->tree, 0);
}

int 	gparser_confirmed(t_gparser *container)
{
	if (!container)
		return -100;
	if (container->parsing_result != 0)
		return container->parsing_result;
	return (container->parsing_result == 0 && container->tree->confirmed);
}



void 	gparser_free(t_gparser *container)
{
	if (!container)
		return;
	free_list_node(&container->backup);
	free_list_node(&container->tree);
#ifdef G_DEBUG_FREE
	printf(" size %d size %d\n", count_list_node(&container->tree), count_list_node(&container->backup));
#endif
}

void 	gparser_build_unlock(t_gparser *container)
{
	if (!container)
		return;
	if (!container->tree)
		return;
	if (!container->callback)
		return;
	iter_tree_confirmed(container->tree, container->callback);
}

/*
 *
 *	Make a call to the function provided by user and assign node as confirmed or not
 *
 *
 *	return 0 if success
 *
 */

int 	gparser(char *str, t_gparser *container)
{
	t_node *tree;
	t_node *backup;
	int parsing;

	tree = 0;
	backup = 0;

	if (!container)
		return -1000;

	container->parsing_result = -1;
	container->tree = 0;
	container->backup = 0;

	if (!container->callback)
	{
		container->parsing_result = -2000;
		return -2000;
	}
	if (!str)
	{
		container->parsing_result = -3000;
		return -3000;
	}

	parsing = gbuild(str, &tree, &backup);

	container->parsing_result = parsing;

	if (parsing != 0)
	{
		free_list_node(&tree);
		free_list_node(&backup);
		container->tree = 0; 
		container->backup =0;
		return (parsing + (-4000));
	}


	iter_tree_callback(tree, container->callback);

#ifdef G_DEBUG
	printf("======== PRINT TREE ==========\n");
	iter_tree(tree,0);
	printf("======== END PRINT TREE ==========\n");

	printf("======== PRINT LIST TREE ==========\n");
	iter_list_node(tree);
	printf("======== END PRINT LIST TREE ==========\n");
#ifdef G_DEBUG_FREE
	printf("======== PRINT LIST BACKUP ==========\n");
	iter_list_node(backup);
	printf("======== END PRINT LIST BACKUP ==========\n");
#endif
#endif	
	container->tree = tree;
	container->backup = backup;
	return parsing;
}

